import os

import setuptools

module_path = os.path.join(os.path.dirname(__file__), 'google_shopping_orm.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = version_line.split('__version__ = ')[-1][1:][:-2]

setuptools.setup(
    name="Google Shopping ORM",
    version=__version__,
    url="https://bitbucket.org/FwPyClassification/fwgoogleorm",

    author="fw",

    description="ORM models, and DB connection code, for the data obtained from Google Shopping",
    long_description=open('README.md').read(),

    py_modules=['google_shopping_orm'],
    zip_safe=False,
    platforms='any',

    install_requires=["sqlalchemy == 1.0.12"],

    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
    ],
)
