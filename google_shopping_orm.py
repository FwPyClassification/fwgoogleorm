__version__ = "0.0.2"

import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Table, ForeignKey
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()

def connectToDb(db_path):
    if db_path:
        engine = create_engine("sqlite:///{}".format(db_path))
    else:
        raise Exception("A path to a database is required!")

    Base.metadata.create_all(engine)
    Session = sessionmaker()
    Session.configure(bind = engine )
    return Session()

class Progress(Base):
    __tablename__ = "progress"
    key = Column( String, primary_key = True )
    value = Column( Integer )


class Category(Base):
    __tablename__ = "categories"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    brands = relationship("Brand", secondary="category_brand", backref="_categories")
    items = relationship("Item", secondary="category_item", backref="_categories")
    other_stuff = relationship("Stuff", secondary="category_stuff", backref="_categories" )
    related_categories = relationship("RelatedCategory", secondary="category_relatedcategory", backref="_categories")
    def __repr__(self):
        return "< Category {} >".format(self.name)

    def __eq__(self, other):
        if self.name == other.name: return True
        else: return False

    def __ne__(self, other):
        if not self.__eq__(other): return True
        else: return False


class RelatedCategory(Base):
    __tablename__ = "related_categories"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    categories = relationship( Category, secondary = "category_relatedcategory", backref="_related_category" )
    def __eq__(self, other):
        if self.name == other.name: return True
        else: return False

    def __ne__(self, other):
        if not self.__eq__(other): return True
        else: return False


class Brand(Base):
    __tablename__= "brands"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    categories = relationship(Category, secondary="category_brand", backref="_brands")
    def __repr__(self):
        return "< Brand {} >".format(self.name)

    def __eq__(self, other):
        if self.name == other.name: return True
        else: return False

    def __ne__(self, other):
        if not self.__eq__(other): return True
        else: return False


class Item(Base):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    price = Column(String)
    url = Column(String)
    categories = relationship(Category, secondary="category_item", backref="_items" )
    def __repr__(self):
        return "< Item {} >".format(self.name)

    def __eq__(self, other):
        if self.name == other.name: return True
        else: return False

    def __ne__(self, other):
        if not self.__eq__(other): return True
        else: return False


class Stuff(Base):
    __tablename__ = "stuff"
    id = Column( Integer, primary_key=True)
    title = Column( String )
    stuff_content = Column( String )
    def __repr__(self):
        return "< {} >".format(self.title)

    def __eq__(self, other):
        if self.title == other.title: return True
        else: return False

    def __ne__(self, other):
        if not self.__eq__(other): return True
        else: return False


class CategoryRelatedCategory(Base):
    __tablename__ = "category_relatedcategory"
    categoryid = Column( Integer, ForeignKey( "categories.id" ), primary_key = True )
    related_categoryid = Column ( Integer, ForeignKey( "related_categories.id" ), primary_key= True )


class CategoryStuff(Base):
    __tablename__ = "category_stuff"
    categoryid = Column( Integer, ForeignKey("categories.id"), primary_key=True)
    stuffid = Column( Integer, ForeignKey("stuff.id"), primary_key=True )


class CategoryBrand(Base):
    __tablename__ = "category_brand"
    categoryid = Column( Integer, ForeignKey("categories.id"), primary_key=True)
    brandid = Column( Integer, ForeignKey("brands.id"), primary_key=True )


class CategoryItem(Base):
    __tablename__ = "category_item"
    categoryid = Column( Integer, ForeignKey("categories.id"), primary_key=True )
    itemid = Column( Integer, ForeignKey("items.id"), primary_key=True)
