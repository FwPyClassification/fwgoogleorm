# ### ORM models and interface, for the data obtained from Google Shopping ### #

Example usage:

```
#!python

import google_shopping_orm as gso

conn = gso.connectToDb("path/to/my.db")
#conn would be a SqlAlchemy Session object
#it supports any query SqlAlchemy does
every_category = conn.query(gso.Category).all()
```